# Sky Betting And Gaming Tech Test API Automation Framework


This repository includes API automation test framework for Sky Betting and Gaming Automation Tech Test implemented using [REST-assured](http://rest-assured.io/) , [Cucumber](https://cucumber.io/) and Java that follows BDD approach.
This will include example step definitions to perform basic calls using common http requests and basic assertions on the responses.
The tests will verify Sky Betting & Gaming Tech Test API available on the docker hub page [on the docker hub page](https://hub.docker.com/r/sbgtechtest/api/),
and further information about terms used, example API responses and the betting domain in general are available in [the background docs](https://github.com/skybet/js-tech-test/blob/master/Background.md) service 

## Prerequisites
* Docker should be installed on your machine 
* Java 11 or 8
* Maven
* IDE



## Running the tests
Run the docker-compose.yml to start the Sky Betting & Gaming Tech Test API
* Right click on the [docker-compose.yml](docker-compose.yml) and select Run
* OR
* Type command on terminal as :  `docker-compose up`


Perform manual test uring postman
* Postman API Collection available [ManualTestAPI-PostmanCollection](./src/test/resources/postman/SKY-Betting&Gaming-TechTestAPI.postman_collection.json) and select Run


Run Automation Cucumber Tests using the cucumber.Options.TestRunner 
* Right click on the [cucumber.Options.TestRunner](./src/test/java/runner/TestRunner.java) and select Run


Run through maven to generate the Cucumber report - can generate both cucumber report via terminal
* `mvn clean verify`


## Overview of feature files
#### Get live football events  (GET requests)
* Trigger GET request call for /football/live API endpoint.
* Verify response status code is 200
* Verify response body is not null
* Verify all records of response body reflect classname as 'Football' indicating all records are Football event
* Verify home’ and an ‘away’ competitor exists for each of the events.
* All assertions are done using Cucumber datatables

