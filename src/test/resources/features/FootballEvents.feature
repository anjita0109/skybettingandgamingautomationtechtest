#=======================================================================================================================
# Author: Anjita Lamzel
# Title: Verify
# Date: 13/02/2022

#=======================================================================================================================

@CR_Betting
Feature: CR_Betting_Test_Cases


#=======================================================================================================================
  Scenario: CR_Betting_Test_Cases Verify that all the events returned are football events, from the /football/live API endpoint.
#=======================================================================================================================

    Given User specifies request structure for an "GetFootballEventAPI" API
    When user performs "GET" request for "GetFootballEventAPI" API
    Then status code 200 is returned
    And a response body should be returned
    And response body should have all 18 records as "Football" events and "home" and "away" competitor exists for each of the events
      | field | value                                                       |
      | name  | Shanghai Shenhua 0 v 0 Shandong Luneng Taishan              |
      | name  | Home United 0 v 1 Albirex Niigata FC                        |
      | name  | Kuching FA 0 v 1 UKM FC                                     |
      | name  | Payasspor 1 v 0 Amed Sportif Faaliyetler                    |
      | name  | Maccabi Ironi Kiryat Ata 0 v 0 Hapoel Shfaram               |
      | name  | Bandirmaspor 0 v 0 Çorum Belediyespor                       |
      | name  | Kaspian Qazvin 0 v 0 Hef Semnan                             |
      | name  | Bodrum Belediyesi Bodrumspor 0 v 2 Gaskispor Gaziantep      |
      | name  | Terengganu FA 0 v 0 Felcra FC                               |
      | name  | Muglaspor 0 v 1 Etimesgut Belediyespor                      |
      | name  | Kirklarelispor 0 v 1 Nigde Belediyespor                     |
      | name  | MOF FC 0 v 0 Sime Darby FC                                  |
      | name  | Shahrdari Hamedan 0 v 0 Khoosheh Talai                      |
      | name  | Yeni Menemen Belediyespor 0 v 1 Gençlerbirligi SK           |
      | name  | Wigan Athletic Reserves 1 v 0 Fleetwood Town Reserves       |
      | name  | Barnet Reserves 0 v 0 Southend United Reserves              |
      | name  | Burton Albion (Reserves) 0 v 3 Mansfield Town FC (Reserves) |
      | name  | Guria Lanchkhuti U19 2 v 0 Meshakhte Tkibuli U19            |

