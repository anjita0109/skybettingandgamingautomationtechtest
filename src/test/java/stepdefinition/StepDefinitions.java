package stepdefinition;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import pages.BasePage;

import java.io.IOException;

public class StepDefinitions extends BasePage {

    private static RequestSpecification reqSpec;


    @Given("User specifies request structure for an {string} API")
    public void userSpecifiesRequestStructureForAnAPI(String apiName) throws IOException {
        reqSpec = requestSpecification();
    }


    @Given("user performs {string} request for {string} API")
    public void userPerformsRequestForAPI(String requestType, String apiName) throws IOException {
        triggerAPIRequest(requestType, apiName, reqSpec);
    }


    @Then("status code {int} is returned")
    public void statusCodeIsReturned(int expectedStatusCode) {
        statusCodeIsValid(expectedStatusCode);
    }


    @And("a response body should be returned")
    public void aResponseBodyShouldBeReturned() {
        responseBodyIsNotNull();
    }



    @And("response body should have all {int} records as {string} events and {string} and {string} competitor exists for each of the events")
    public void responseBodyShouldHaveAllRecordsAsEventsAndAndCompetitorExistsForEachOfTheEvents(int count, String eventType,String home,String away,DataTable jsonTable) throws IOException {
        assertTotalFootballEvents(count, eventType,home,away,jsonTable);
    }
}
