package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class utils {

    public static String getTestEnvironmentBaseURI(String key) throws IOException {
        Properties properties = new Properties();
        System.out.println(System.getProperty("user.dir"));
        FileInputStream fileInputStream= new FileInputStream(System.getProperty("user.dir")+"/src/test/java/config/appConfigFile.properties");
        properties.load(fileInputStream);
        return(properties.getProperty(key));
    }
}
