package utils;

public enum APIResourcePath{

    GetFootballEventAPI("/football/live"),
    GetSportsBookEvent("/sportsbook/even");

    private final String runtimeResourcePath;

    APIResourcePath(String resourcePath)
    {
        runtimeResourcePath = resourcePath;
    }

    public String getResourcePath()
    {
        return runtimeResourcePath;
    }
}
