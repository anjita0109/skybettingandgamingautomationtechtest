package runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions
        (
                features = "src/test/resources/features",
                glue = {"stepdefinition"},
                plugin = {
                        "pretty",
                        "json:target/cucumber.json",
                        "html:target/site/cucumber-pretty"
                }
        )
public class TestRunner {
}

