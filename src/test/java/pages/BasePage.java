package pages;


import io.cucumber.datatable.DataTable;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;
import utils.APIResourcePath;
import java.io.*;
import java.util.List;
import java.util.Map;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.not;
import static utils.utils.getTestEnvironmentBaseURI;




public class BasePage {

    private static RequestSpecification reqSpec;
    private static Response response;
    private static APIResourcePath objAPIResourcePath;
    private String runTimeResourcePath;


    public RequestSpecification requestSpecification() throws IOException {
        if (reqSpec == null) {
            PrintStream stream = new PrintStream(new FileOutputStream("acceptanceTestLogs.txt"));
            reqSpec = new RequestSpecBuilder()
                    .setBaseUri(getTestEnvironmentBaseURI("BaseURI"))
                    .addFilter(RequestLoggingFilter.logRequestTo(stream))
                    .addFilter(ResponseLoggingFilter.logResponseTo(stream))
                    .setContentType(ContentType.JSON)
                    .build();}
        return reqSpec;
    }





    public Response triggerAPIRequest(String requestType, String apiName, RequestSpecification reqSpec) {
        objAPIResourcePath = APIResourcePath.valueOf(apiName);
        if (requestType.equalsIgnoreCase("GET")) {
            response = given().spec(reqSpec.relaxedHTTPSValidation()).get(objAPIResourcePath.getResourcePath());}
        return response;}




    public void statusCodeIsValid(int expectedStatusCode) {
        Assert.assertEquals(expectedStatusCode, response.getStatusCode());
        response.then().assertThat().body(is(notNullValue()));
        response.then().assertThat().body(is(not("")));}




    public void responseBodyIsNotNull() {
        response.then().assertThat().body(is(notNullValue()));
        response.then().assertThat().body(is(not("")));}




    public void assertTotalFootballEvents(int totalRecords, String expectedEventType, String home, String away, DataTable datatable) throws IOException {
        int i = 0;
        JsonPath jsonPath = new JsonPath(response.body().asInputStream());
        List<Map<String, String>> list = datatable.asMaps(String.class, String.class);

        for (Map<String, String> dataTableMap : list) {
            String field = dataTableMap.get("field");
            String value = dataTableMap.get("value");

            String actualValue = jsonPath.getString("events[" + i + "]." + field);
            String eventType = jsonPath.getString("events[" + i + "].className");
            String homeField = jsonPath.getString("events[" + i + "].scores.home");
            String awayField = jsonPath.getString("events[" + i + "].scores.away");


            Assert.assertEquals(value, actualValue);
            Assert.assertEquals(expectedEventType, eventType);
            Assert.assertTrue(!homeField.isEmpty());
            Assert.assertTrue(!awayField.isEmpty());
            i++;}
            Assert.assertEquals(totalRecords, i);}

}
